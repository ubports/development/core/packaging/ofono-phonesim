#!/bin/sh
set -e

MYPATH=`readlink -f $0`
MYDIR=`dirname $MYPATH`

assert_in() {
    if ! echo "$2" | grep -q "$1"; then
        echo "FAIL: '$1' not found in:" >&2
        echo "$2" >&2
        echo "--------" >&2
        exit 1
    fi
}

# wait until modem is present and powered
for timeout in `seq 10`; do
    if /usr/share/ofono/scripts/list-modems | grep -q "org.ofono.MessageManager"; then
        break
    fi
    echo "Waiting for modem to initialize SMS manager..."
    sleep 1
done
assert_in "/phonesim" "`/usr/share/ofono/scripts/list-modems`"

# start SMS monitor
LOG=`mktemp`
export PYTHONUNBUFFERED=x
stdbuf -o 0 /usr/share/ofono/scripts/receive-sms > "$LOG" &
LOG_PID=$!
# receive-sms is a Python script, give it some time to settle
sleep 10

# send SMS through GUI script; do this as unprivileged user
sudo su -s /bin/sh <<EOF games
  export DBUS_SESSION_BUS_ADDRESS=\`cat /run/lock/ofono-phonesim-dbus.address\`
  dbus-send --session --print-reply --dest=org.ofono.phonesim / \
     org.ofono.phonesim.Script.SetPath string:$MYDIR/scripts
  dbus-send --session --print-reply --dest=org.ofono.phonesim / \
     org.ofono.phonesim.Script.Run string:sms.js
EOF

# verify that it arrived
sleep 10
kill $LOG_PID
wait $LOG_PID || true
L=`cat "$LOG"`
rm $LOG
echo "---- log -----"
echo "$L"
echo "--------------"
assert_in "Sender = 0815" "$L"
assert_in "Generated Message" "$L"
